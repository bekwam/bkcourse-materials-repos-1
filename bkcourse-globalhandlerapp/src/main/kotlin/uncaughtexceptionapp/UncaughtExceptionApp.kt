package uncaughtexceptionapp

import javafx.geometry.Insets
import javafx.scene.Scene
import javafx.scene.control.Alert
import tornadofx.*

class FooSubsystem {
    fun foo() { throw IllegalStateException("unchecked exception from foo") }
}

class BarSubsystem {
    fun bar() { throw IllegalArgumentException("unchecked exception from bar")}
}

class OkSubsystem {
    fun ok() {}
}

class UncaughtExceptionController : Controller() {

    val fooSubsystem = FooSubsystem()
    val barSubsystem = BarSubsystem()
    val okSubsystem = OkSubsystem()

    fun foo() = fooSubsystem.foo()
    fun bar() = barSubsystem.bar()
    fun ok() = okSubsystem.ok()
}

class UncaughtExceptionView : View("Uncaught Exception App") {

    val controller : UncaughtExceptionController by inject()

    override val root = hbox {

        button("Foo") {
            action {

                controller.foo()

                alert(Alert.AlertType.INFORMATION, "Success", "Foo Completed!" )
            }
        }

        button("Bar") {
            action {

                controller.bar()

                alert(Alert.AlertType.INFORMATION, "Success", "Bar Completed!" )
            }
        }

        button("Ok") {
            action {

                controller.ok()

                alert(Alert.AlertType.INFORMATION, "Success", "Ok Completed!" )
            }
        }

        padding = Insets(10.0)
        spacing = 4.0
    }

}

class UncaughtExceptionApp : App(UncaughtExceptionView::class) {
    override fun createPrimaryScene(view: UIComponent): Scene = Scene(view.root, 568.0, 320.0)
}

fun main(args : Array<String>) {
    launch<UncaughtExceptionApp>(args)
}