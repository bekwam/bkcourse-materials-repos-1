package globalhandlerapp

import com.google.inject.AbstractModule
import com.google.inject.Guice
import com.google.inject.matcher.Matchers
import javafx.application.Platform
import javafx.geometry.Insets
import javafx.scene.Scene
import javafx.scene.control.Alert
import org.aopalliance.intercept.MethodInterceptor
import org.aopalliance.intercept.MethodInvocation
import tornadofx.*

open class FooSubsystem {
    open fun foo() { throw IllegalStateException("unchecked exception from foo") }
}

open class BarSubsystem {
    open fun bar() { throw IllegalArgumentException("unchecked exception from bar")}
}

open class OkSubsystem {
    open fun ok() {}
}

class SubsystemResultHandler : MethodInterceptor {

    override fun invoke(invocation: MethodInvocation?): Any {

        try {

            val retval = invocation!!.proceed()  // could be off thread

            Platform.runLater({
                success("${invocation!!.method.name.capitalize()} Completed!")
            })

            if( retval == null ) {
                return Any()
            }

        } catch(exc : Exception) {
            Platform.runLater({
                fatalAlert("There was an error calling ${invocation!!.method.name.capitalize()}")
            })
        }

        return Any()
    }

    private fun fatalAlert(specificMessage : String) =
            alert(Alert.AlertType.ERROR, "System Error", specificMessage)

    private fun success(specificMessage : String) =
            alert(Alert.AlertType.INFORMATION, "Success", specificMessage)
}

class GlobalHandlerModule : AbstractModule() {
    override fun configure() {
        bindInterceptor(
                Matchers.inPackage(Package.getPackage("globalhandlerapp")),
                Matchers.any(),
                SubsystemResultHandler()
        )
    }
}

class GlobalHandlerController : Controller() {

    val fooSubsystem : FooSubsystem
    val barSubsystem : BarSubsystem
    val okSubsystem : OkSubsystem

    fun foo() = fooSubsystem.foo()
    fun bar() = barSubsystem.bar()
    fun ok() = okSubsystem.ok()

    init {

        val injector = Guice.createInjector(GlobalHandlerModule())

        fooSubsystem = injector.getInstance(FooSubsystem::class.java)
        barSubsystem = injector.getInstance(BarSubsystem::class.java)
        okSubsystem = injector.getInstance(OkSubsystem::class.java)
    }
}

class GlobalHandlerView : View("Global Handler App") {

    val controller : GlobalHandlerController by inject()

    override val root = hbox {

        button("Foo") {
            action {
                controller.foo()
            }
        }

        button("Bar") {
            action {
                controller.bar()
            }
        }

        button("Ok" ) {
            action {
                controller.ok()
            }
        }

        padding = Insets(10.0)
        spacing = 4.0
    }
}

class GlobalHandlerApp : App(GlobalHandlerView::class) {
    override fun createPrimaryScene(view: UIComponent): Scene = Scene(view.root, 568.0, 320.0)
}

fun main(args : Array<String>) {
    launch<GlobalHandlerApp>(args)
}