/*
 * Copyright 2016 Bekwam, Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bekwam.bkcourse.confirmapp;

import javafx.animation.FadeTransition;
import javafx.beans.binding.*;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.util.Duration;
import org.apache.commons.validator.routines.EmailValidator;

import java.util.concurrent.Callable;
import java.util.logging.Logger;

/**
 * @author carl
 */
public class ConfirmController {

    private Logger logger = Logger.getLogger("ConfirmController");

    @FXML
    private TextField txtEmail, txtConfirmEmail;

    @FXML
    private GridPane gridPane;

    @FXML
    private Button btnSave;

    @FXML
    private Label lblValid;

    @FXML
    private VBox vboxValid;

    @FXML
    public void initialize() {

        EmailValidator emailValidator = EmailValidator.getInstance();

        StringBinding validEmailExpr = Bindings.createStringBinding(
                () -> emailValidator.isValid(txtEmail.getText())?txtEmail.getText():"",
                txtEmail.textProperty()
        );

        btnSave.disableProperty().bind(
            txtEmail.textProperty()
                .isEqualTo(txtConfirmEmail.textProperty()).not()
                .or(
                    txtEmail.textProperty().isEqualTo( validEmailExpr ).not()
                )
                .or(
                    txtEmail.textProperty().isEmpty()
                )
        );

        BooleanBinding okToAnimate =  txtEmail.textProperty()
                .isEqualTo( validEmailExpr ).not()
                .or(
                        txtEmail.textProperty().isEqualTo(txtConfirmEmail.textProperty()).not()
                );

        okToAnimate.addListener( (obs, ov, nv) -> {

            logger.info("txtEmail changed from " + ov + " to " + nv);

            FadeTransition ft = null;

            if( nv ) { // ok to show -> fade in
                ft = new FadeTransition(Duration.millis(500), vboxValid);
                ft.setFromValue(0.0d);
                ft.setToValue(1.0d);
            } else {

                if( !txtEmail.getText().isEmpty() ) {
                    vboxValid.getStyleClass().clear();
                    vboxValid.getStyleClass().add("vbox-valid-ok");
                    lblValid.getStyleClass().clear();
                    lblValid.getStyleClass().add("label-valid-ok");
                    lblValid.setText("=");
                }

                ft = new FadeTransition(Duration.millis(1000), vboxValid);
                ft.setFromValue(1.0d);
                ft.setToValue(0.0d);
                ft.setOnFinished( (evt) -> {

                            if( !txtEmail.getText().isEmpty() ) {
                                vboxValid.getStyleClass().clear();
                                vboxValid.getStyleClass().add("vbox-valid-error");
                                lblValid.getStyleClass().clear();
                                lblValid.getStyleClass().add("label-valid-error");
                                lblValid.setText("X");
                            }
                });
            }
            ft.play();
        } );
    }

    @FXML
    public void save(ActionEvent evt) {

        logger.info("confirmed " + txtEmail.getText());

        hide(evt);
    }

    @FXML
    public void cancel(ActionEvent evt) {

        hide(evt);
    }

    private void hide(ActionEvent evt) {
        ((Node)evt.getSource()).getScene().getWindow().hide();
    }
}
