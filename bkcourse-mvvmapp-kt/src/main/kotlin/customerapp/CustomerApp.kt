package customerapp

import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleDoubleProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import javafx.collections.ObservableList
import javafx.concurrent.Task
import javafx.geometry.Insets
import javafx.scene.Scene
import tornadofx.*
import javax.json.JsonObject

/**
 * MVVM example
 *
 * 1. Loading Data
 *
 * Refresh in the CustomerListFragment initiates a loadCustomers operation on
 * a RESTful web service.  The CustomerListFragment delegates to
 * CustomerViewModel.  The ViewModel makes an asynchronous call to
 * CustomerModel.
 *
 * After some time, the CustomerModel posts a CustomerModelUpdated event to
 * interested listeners.  In this case, there is only one interested object,
 * CustomerViewModel.
 *
 * CustomerViewModel receives the event and retrieves a fresh list from
 * CustomerModel.  Notice that this retrieval -- direct access of the customers
 * property -- is a direct RAM access and does not prompt the model to
 * go to the RESTful service again.
 *
 * 2. Selecting Data
 *
 * This isn't a Model change, but a selection on CustomerListFragment will
 * change a ViewModel item.  This way, the bound value of the other Fragment,
 * CustomerDetailsFragment, can update itself.  This avoids a coupling between
 * parent and child (CustomerView -> CustomerListFragment +
 * CustomerDetailsFragment) and between child and child (CustomerListFragment
 * -> CustomerDetailsFragment)
 *
 * @author carl
 */
data class Customer(val id : Int, val firstName : String, val lastName : String) {
    override fun toString(): String {
        return "$firstName $lastName";
    }
}

object CustomerModelUpdated : FXEvent()  // thrown Model -> ViewModel

class MainView : View("Customer App") {

    val customerViewModel : CustomerViewModel by inject()

    override val root =
        vbox {
            splitpane {
                this += find(CustomerListFragment::class)
                this += find(CustomerDetailsFragment::class)
                padding = Insets(4.0)
            }

            hbox {
                progressbar {
                    progressProperty().bind( customerViewModel.taskProgress )
                }
                label {
                    textProperty().bind( customerViewModel.taskMessage )
                }
                visibleProperty().bind( customerViewModel.taskRunning )
                padding = Insets(4.0)
                spacing = 4.0
            }
    }
}

class CustomerListFragment : Fragment() {

    val customerViewModel : CustomerViewModel by inject()

    override val root = vbox {
        button("Refresh") {
            setOnAction { refresh() }
        }
        listview<Customer> {
            itemsProperty().bind( customerViewModel.customers )
            selectionModel.selectedItemProperty().addListener {
                obs, ov, nv ->
                    updateSelected(nv)
            }
        }

        padding = Insets(10.0)
        spacing = 4.0
    }

    fun refresh() {
        customerViewModel.refresh()
    }

    fun updateSelected(newSelection : Customer?) {
        customerViewModel.updateSelected(newSelection)
    }
}

class CustomerDetailsFragment : Fragment() {

    val customerViewModel : CustomerViewModel by inject()

    override val root = vbox {
        label("First Name")
        textfield {
            textProperty().bind( customerViewModel.selectedFirstName )
        }
        label("Last Name")
        textfield {
            textProperty().bind( customerViewModel.selectedLastName )
        }
        padding = Insets(10.0)
    }
}

class CustomerViewModel : ViewModel() {

    val customerModel : CustomerModel by inject()

    val customers = SimpleObjectProperty<ObservableList<Customer>>()
    val selectedFirstName = SimpleStringProperty()
    val selectedLastName = SimpleStringProperty()

    val taskRunning = SimpleBooleanProperty()
    val taskMessage = SimpleStringProperty()
    val taskProgress = SimpleDoubleProperty()

    init {
        subscribe<CustomerModelUpdated> {
            updateFromRefresh()
        }
    }

    fun refresh() {

        val t = object : Task<Unit>() {
            override fun call() {
                updateMessage("Loading customers")
                updateProgress( 0.4, 1.0 )
                customerModel.loadCustomers()
            }
            override fun succeeded() {
                fire( CustomerModelUpdated );
            }
        }

        taskRunning.bind( t.runningProperty() )
        taskProgress.bind( t.progressProperty() )
        taskMessage.bind( t.messageProperty() )

        Thread(t).start()
    }

    fun updateSelected(newSelection : Customer?) {
        if( newSelection == null ) {
            selectedFirstName.set("")
            selectedLastName.set("")
        } else {
            selectedFirstName.set( newSelection!!.firstName )
            selectedLastName.set( newSelection!!.lastName )
        }
    }

    fun updateFromRefresh() {

        customers.set(
            customerModel.customers.observable()
        )
    }
}

class CustomerModel : Controller() {

    val api : Rest by inject()

    val customers = mutableListOf<Customer>()

    fun loadCustomers() {

        customers.clear()

        api.get("customers.json")
                .list()
                .forEach{
                    val obj = it as JsonObject
                    customers.add(
                        Customer(
                            obj.getInt("id"),
                            obj.getString("firstName"),
                            obj.getString("lastName")
                        )
                    )
                }

        fire( CustomerModelUpdated )
    }
}

class CustomerApp : App(MainView::class) {

    val api : Rest by inject()

    override fun createPrimaryScene(view: UIComponent) =
            Scene(view.root, 568.0, 320.0 )

    init {
        api.baseURI = "https://www.bekwam.net/data"
    }
}