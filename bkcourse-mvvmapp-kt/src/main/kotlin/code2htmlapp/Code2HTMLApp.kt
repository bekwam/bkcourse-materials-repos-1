package code2htmlapp

import javafx.beans.property.SimpleStringProperty
import javafx.scene.Scene
import tornadofx.*

/**
 * Created by carl_000 on 1/7/2017.
 */

class Code2HTMLConverter : Component(), Injectable {
    fun convertCode(code : String) : String {

        var s = code

        s = s.replace("\t", " ")
        s = s.replace("<", "&lt;")
        s = s.replace(">", "&gt;")

        if( s.contains("\r\n") ) {  // win
            s = s.lines().fold("") { result, elem -> result + formatLeadingSpaces(elem) + "\r\n" }
            s = s.replace("\r\n", "<br>\r\n")
        } else {  // mac, linux
            s = s.lines().fold("") { result, elem -> result + formatLeadingSpaces(elem) + "\n" }
            s = s.replace("\n", "<br>\n", true)
        }

        return s
    }

    fun formatLeadingSpaces(input : String) : String {
        var retval = ""
        val numSpaces = calcNumSpaces( input )
        (0..numSpaces-1).forEach { retval += "&nbsp;" }
        retval += input.trimStart()
        return retval
    }

    fun calcNumSpaces(input : String ) : Int {
        var retval = 0
        val r = Regex("^([ ]+)").find( input )
        if( r != null ) {
            retval = r.value.length
        }
        return retval
    }
}

class Code2HTMLViewModel : ViewModel() {

    val converter : Code2HTMLConverter by inject()

    val code = SimpleStringProperty()
    val html = SimpleStringProperty()

    fun convertCode() {
        html.set( converter.convertCode(code.get()) )
    }
}

class Code2HTMLView : View("Code2HTML") {

    val viewModel : Code2HTMLViewModel by inject()

    override val root =
            tabpane {
                tab("Code") {
                    textarea(viewModel.code)
                }
                tab("HTML") {
                    textarea(viewModel.html) {
                     isEditable = false
                    }
                }
                selectionModel.selectedItemProperty().addListener {
                    observableValue, ov, nv ->
                    if (nv.text == "HTML") {
                        viewModel.convertCode()
                    }
                }
            }
}

class Code2HTMLApp : App(Code2HTMLView::class) {

    override fun createPrimaryScene(view: UIComponent) =
            Scene(view.root, 736.0, 414.0)
}