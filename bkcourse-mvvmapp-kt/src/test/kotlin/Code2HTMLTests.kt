import code2htmlapp.Code2HTMLConverter
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.jetbrains.spek.api.dsl.on
import org.junit.Assert.assertEquals

/**
 * Created by carl_000 on 1/8/2017.
 */

object Code2HTMLConverterSpec : Spek({

    describe("code to html converter") {

        val converter = Code2HTMLConverter()

        on("class def - win") {
            val data = "class MyClass\r\n" +
                    "  String field1\r\n" +
                    "  fun funcA() {\r\n" +
                    "    val s1 = \"test\"\r\n"
            val result = converter.convertCode( data )
            it( "converted code for win" ) {

                val expected = "class MyClass<br>\r\n" +
                        "&nbsp;&nbsp;String field1<br>\r\n" +
                        "&nbsp;&nbsp;fun funcA() {<br>\r\n" +
                        "&nbsp;&nbsp;&nbsp;&nbsp;val s1 = \"test\"<br>\r\n<br>\r\n"

                assertEquals( expected, result )
            }
        }

        on("class def - macos or linux") {
            val data = "class MyClass\n" +
                    "  String field1\n" +
                    "  fun funcA() {\n" +
                    "    val s1 = \"test\"\n"
            val result = converter.convertCode( data )
            it( "converted code for win" ) {

                val expected = "class MyClass<br>\n" +
                        "&nbsp;&nbsp;String field1<br>\n" +
                        "&nbsp;&nbsp;fun funcA() {<br>\n" +
                        "&nbsp;&nbsp;&nbsp;&nbsp;val s1 = \"test\"<br>\n<br>\n"

                assertEquals( expected, result )
            }
        }
    }
})
