/**
 * @author carl
 */
import javafx.application.Application
import javafx.concurrent.Task
import javafx.event.EventHandler
import javafx.fxml.FXML
import javafx.fxml.FXMLLoader
import javafx.scene.Parent
import javafx.scene.Scene
import javafx.scene.control.TableColumn
import javafx.scene.control.TableView
import javafx.scene.control.cell.PropertyValueFactory
import javafx.stage.Stage

/**
 * @author carl
 */

data class Participant(val screenName: String)

class SimpleTableController {

    @FXML var tcScreenName = TableColumn<Participant, String>()

    @FXML var tblParticipants = TableView<Participant>()

    fun initialize() {
        tcScreenName.cellValueFactory = PropertyValueFactory<Participant, String>("screenName")
    }

    fun refresh() {

        val task = object : Task<List<Participant>> () {
            override fun call(): List<Participant> {
                return fetchData()
            }

            override fun succeeded() {
                tblParticipants.items.clear()
                tblParticipants.items.addAll( value )
            }
        }

        Thread(task).start()

    }

    var counter = 1

    fun fetchData() : List<Participant> {

        val participants = listOf(
                Participant("Carl" + counter++),
                Participant("Wanda" + counter++),
                Participant("Peter" + counter++)
        )

        return participants
    }
}

class SimpleTableApp : Application() {

    override fun start(primaryStage: Stage?) {

        val fxmlLoader = FXMLLoader( SimpleTableApp::class.java.getResource("/SimpleTable.fxml"))

        val parent : Parent = fxmlLoader.load()

        val c : SimpleTableController = fxmlLoader.getController()

        val scene = Scene( parent )

        primaryStage!!.scene = scene
        primaryStage.title = "SimpleTableApp"
        primaryStage.width = 667.0
        primaryStage.height = 375.0
        primaryStage.onShown = EventHandler { c.refresh() }
        primaryStage.show()

    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            launch(SimpleTableApp::class.java)
        }
    }
}
