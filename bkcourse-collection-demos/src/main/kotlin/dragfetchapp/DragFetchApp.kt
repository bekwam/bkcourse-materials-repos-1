package dragfetchapp

import javafx.geometry.Pos
import javafx.scene.layout.Priority
import javafx.scene.paint.Color
import tornadofx.*
import java.io.BufferedReader
import java.io.InputStreamReader
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

data class Message(val data : String, val ts : LocalDateTime)

class FetchCompletedEvent : FXEvent()

class DragFetchView : View("Drag Fetch App") {

    val c : DragFetchController by inject()

    override val root = stackpane {

        vbox {
            label("Fetching")
            progressindicator()
            alignment = Pos.TOP_CENTER
            spacing = 4.0
            paddingTop = 10.0
        }

        listview(c.msgs) {

            var inDrag = false
            setOnDragDetected {
                this.translateYProperty().animate(endValue = 100.0, duration = .3.seconds)
                inDrag = true
            }
            setOnMouseReleased {
                if( inDrag && !c.status.running.value ) {
                    c.fetchAsync()
                }
                inDrag = false
            }

            placeholder = label("Drag to fetch messages...")
            cellFormat {
                text = null
                graphic = hbox {
                    addClass(Styles.message)
                    label(it.data).addClass(Styles.messageText)
                    pane().hgrow = Priority.ALWAYS
                    label(it.ts.format(DateTimeFormatter.ofPattern("hh:mm:ss"))).addClass(Styles.messageTime)
                    alignment = Pos.BASELINE_CENTER
                }
            }
            subscribe<FetchCompletedEvent> {
                this@listview.translateYProperty().animate(endValue = 0.0, duration = .3.seconds)
            }
        }
    }

    override fun onDock() {
        primaryStage.minWidth = 320.0
        primaryStage.minHeight = 568.0
    }


}

class DragFetchController : Controller() {

    val api : Rest by inject()

    val msgs = mutableListOf<Message>().observable()

    val status : TaskStatus by inject()

    val NUM_MESSAGES = 10
    var counter = 0 // %10 cycles over the 10 messages on the server

    init {
        api.baseURI = "https://www.bekwam.net/data"
    }

    private fun fetch() : String? {
        val response = api.get("/messages.txt")
        if( response.ok() ) {
            val br = BufferedReader(InputStreamReader(response.content()))
            br.use {
                var s = br.readLine()
                val index = counter % NUM_MESSAGES
                var i = 0
                while( s != null && (i<index) ) {
                    s = br.readLine()
                    i++
                }
                counter++
                return s
            }
        }
        return null
    }

    fun fetchAsync() {
        runAsync {
            fetch()
        } ui {
            if( it != null ) {
                msgs.add(Message(it, LocalDateTime.now()))
            }
            fire(FetchCompletedEvent())
        } fail {
            println( it )
        }
    }
}

class Styles : Stylesheet() {
    companion object {
        val progressBox by cssclass()
        val message by cssclass()
        val messageText by cssclass()
        val messageTime by cssclass()
    }

    init {
        root {
            baseColor = Color.BLACK
        }
        listView {
            backgroundColor += Color.gray(0.2)

            label {
                textFill = Color.WHITE
            }

            s(listCell, listCell and even, listCell and odd) {
                backgroundColor += Color.TRANSPARENT
            }
            message {
                padding = box(5.px)
                borderWidth += box(0.px, 0.px, 1.px, 0.px)
                borderColor += box(Color.gray(1.0, 0.1))
                messageText {
                    padding = box(0.px, 0.px, 5.px, 0.px)
                }
                messageTime {
                    fontSize = 0.75.em
                    opacity = 0.5
                }
            }
        }
    }
}

class DragFetchApp : App(DragFetchView::class, Styles::class)

fun main(args: Array<String>) {
    launch<DragFetchApp>(args)
}

