/*
 * Copyright 2016 Bekwam, Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bekwam.bkcourse.choicesapp;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventType;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import javafx.util.Pair;
import javafx.util.StringConverter;

import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.List;

/**
 * @author carl
 */

class CustomStringConverter extends StringConverter<Pair<String,String>> {

    private boolean useUpper = false;

    @Override
    public String toString(Pair<String, String> pair) {
        if( useUpper ) {
            return pair.getKey().toUpperCase();
        }
        return pair.getKey();
    }

    @Override
    public Pair<String, String> fromString(String string) {
        return null;
    }
    public void setUseUpper(boolean useUpper) {
        this.useUpper = useUpper;
    }

    public boolean getUseUpper() { return useUpper;}
}

public class ChoicesApp extends Application {

    private final ChoiceBox<Pair<String,String>> assetClass = new ChoiceBox<>();

    private final static Pair<String, String> EMPTY_PAIR = new Pair<>("", "");

    private final CustomStringConverter converter = new CustomStringConverter();

    @Override
    public void start(Stage primaryStage) throws Exception {

        Label label = new Label("Asset Class:");
        assetClass.setPrefWidth(200);
        Button saveButton = new Button("Save");
        Button replaceItemButton = new Button("Replace");
        replaceItemButton.setOnAction( this::replaceItem );
        Button switchConverter = new Button("Converter");
        switchConverter.setOnAction( this::switchConverter );

        HBox hbox = new HBox(
                label,
                assetClass,
                saveButton,
                replaceItemButton,
                switchConverter);
        hbox.setSpacing( 10.0d );
        hbox.setAlignment(Pos.CENTER );
        hbox.setPadding( new Insets(40) );

        Scene scene = new Scene(hbox);

        initChoice();

        saveButton.setOnAction(
                (evt) -> System.out.println("saving " + assetClass.getValue())
        );

        primaryStage.setTitle("ChoicesApp");
        primaryStage.setScene( scene );
        primaryStage.show();

    }

    private void replaceItem(ActionEvent evt) {
        FXCollections.replaceAll( assetClass.getItems(), new Pair("Investment", "22000"), new Pair("INV", "22001"));
    }

    private void switchConverter(ActionEvent evt) {
        converter.setUseUpper(!converter.getUseUpper());

        //assetClass.fireEvent(new Event(assetClass, assetClass, new EventType("converter")));
/*
        assetClass.setConverter( null );
        assetClass.setConverter( converter );
*/
    }

    private void initChoice() {

        List<Pair<String, String>> assetClasses = new ArrayList<>();
        assetClasses.add(new Pair("Equipment", "20000"));
        assetClasses.add(new Pair("Furniture", "21000"));
        assetClasses.add(new Pair("Investment", "22000"));

        assetClass.setConverter( converter );

        assetClass.getItems().add( EMPTY_PAIR );
        assetClass.getItems().addAll(assetClasses);
        assetClass.setValue( EMPTY_PAIR );

    }

    public static void main(String[] args) {
        launch(args);
    }
}
