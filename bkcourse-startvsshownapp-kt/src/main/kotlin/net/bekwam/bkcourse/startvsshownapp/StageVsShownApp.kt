package net.bekwam.bkcourse.startvsshownapp

import javafx.beans.binding.Bindings
import javafx.beans.property.SimpleDoubleProperty
import javafx.beans.property.SimpleStringProperty
import javafx.geometry.Pos
import javafx.scene.Scene
import tornadofx.*

/**
 * @author carl
 */

class StartVsShownApp : App(StartVsShownView::class) {

    override fun createPrimaryScene(view: UIComponent) = Scene(view.root, 480.0, 320.0 )

}

class StartVsShownView : View("Start Vs. Shown") {

    val startX = SimpleDoubleProperty()
    val startY = SimpleDoubleProperty()
    val shownX = SimpleDoubleProperty()
    val shownY = SimpleDoubleProperty()

    val dimensionsStart = SimpleStringProperty()
    val dimensionsShown = SimpleStringProperty()

    override val root = vbox {
        hbox {
            gridpane {
                row {
                    label("Start Dimensions")
                    textfield(dimensionsStart)
                }
                row {
                    label("Shown Dimensions")
                    textfield(dimensionsShown)
                }
                hgap = 10.0
                vgap = 10.0
            }
            alignment = Pos.CENTER
        }
        alignment = Pos.CENTER
    }

    init {

        dimensionsStart.bind( Bindings.format("(%.1f,%.1f)", startX, startY) )
        dimensionsShown.bind( Bindings.format("(%.1f,%.1f)", shownX, shownY) )

        startX.value = primaryStage.width
        startY.value = primaryStage.height

        primaryStage.setOnShown {
            shownX.value = primaryStage.width
            shownY.value = primaryStage.height
        }
    }
}