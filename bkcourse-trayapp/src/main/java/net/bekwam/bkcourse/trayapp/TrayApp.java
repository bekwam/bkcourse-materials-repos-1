/*
 * Copyright 2016 Bekwam, Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bekwam.bkcourse.trayapp;

import io.netty.bootstrap.Bootstrap;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.binding.When;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.concurrent.ScheduledService;
import javafx.concurrent.Task;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.apache.http.client.fluent.Request;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.net.InetSocketAddress;
import java.net.URL;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

/**
 * @author carl
 */
public class TrayApp extends Application {

    private final static int YELLOW_MISSES = 10;
    private final static int RED_MISSES = 20;
    public static final int SERVER_CHECK_INTERVAL = 2;

    private static Thread systemTrayThread;
    private static Thread fxServerThread;

    private static final Map<String, Image> trayIconSet = new LinkedHashMap<>();

    private final Map<String, javafx.scene.image.Image> appIconSet = new LinkedHashMap<>();

    private static int missesCounter = 0;

    private ObjectProperty<TrayAppStatusType> trayAppStatus = new SimpleObjectProperty<>(TrayAppStatusType.DEAD);

    private ChannelFuture fxf;

    @Override
    public void start(Stage primaryStage) throws Exception {

        loadAppIconSet();

        Platform.setImplicitExit(true);

        StackPane sp = new StackPane();

        Label label = new Label("TrayApp");
        ImageView imageView = new ImageView();
        Label statusLabel = new Label("");

        statusLabel.textProperty().bind( trayAppStatus.asString() );

        imageView.imageProperty().bind(
                    new When(
                            trayAppStatus.isEqualTo(TrayAppStatusType.ALIVE) ).
                            then( appIconSet.get("green") ).
                            otherwise(
                                    new When( trayAppStatus.isEqualTo(TrayAppStatusType.INTERRUPTED)).
                                            then( appIconSet.get("yellow") ).
                                            otherwise(
                                                    new When( trayAppStatus.isEqualTo(TrayAppStatusType.DYING)).
                                                            then(appIconSet.get("red")).
                                                            otherwise(
                                                                appIconSet.get("gray")
                                                            )
                                            )
                            )
            );

        HBox hbox = new HBox();
        hbox.setPadding(new javafx.geometry.Insets(10));
        hbox.getChildren().addAll( statusLabel, imageView );
        hbox.setAlignment(Pos.BOTTOM_RIGHT);

        sp.getChildren().addAll( label, hbox );

        Scene scene = new Scene(sp);

        primaryStage.setWidth( 800 );
        primaryStage.setHeight( 600 );
        primaryStage.setTitle("TrayApp");
        primaryStage.setScene( scene );
        primaryStage.setOnShown( (evt) -> {

            fxServerThread = new Thread( () -> createFXServerThread() );
            fxServerThread.start();

        });

        primaryStage.setOnHiding( (evt) -> {

            //systemTrayThread.interrupt();
        });
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

    private void createFXServerThread() {

        try {
            final int port = 15000;

            final AppServerHandler serverHandler = new AppServerHandler();
            EventLoopGroup group = new NioEventLoopGroup();

            try {
                ServerBootstrap b = new ServerBootstrap();
                b.group(group).channel(NioServerSocketChannel.class)
                        .localAddress(new InetSocketAddress(port))
                        .childHandler(new ChannelInitializer<SocketChannel>() {
                            @Override
                            protected void initChannel(SocketChannel ch) throws Exception {
                                ch.pipeline().addLast(serverHandler);
                            }
                        });

                ChannelFuture f = b.bind().sync();
                f.addListener(new ChannelFutureListener() {
                    @Override
                    public void operationComplete(ChannelFuture future) throws Exception {
                        ScheduledService<Boolean> serverCheck = new ScheduledService<Boolean>() {
                            private int numMisses = 0;

                            @Override
                            protected Task<Boolean> createTask() {
                                Task<Boolean> aliveTask = new Task<Boolean>() {

                                    @Override
                                    protected Boolean call() throws Exception {
                                        return isAlive();
                                    }

                                    @Override
                                    protected void succeeded() {

                                        if( getValue() ) { // alive

                                            trayAppStatus.set(TrayAppStatusType.ALIVE);
                                            numMisses = 0;

                                        } else {

                                            if( missesCounter < YELLOW_MISSES ) {
                                                trayAppStatus.set(TrayAppStatusType.INTERRUPTED);
                                            } else if( missesCounter < RED_MISSES ) {
                                                trayAppStatus.set(TrayAppStatusType.DYING);
                                            } else {
                                                trayAppStatus.set(TrayAppStatusType.DEAD);
                                            }

                                            numMisses++;
                                        }
                                    }
                                };
                                return aliveTask;
                            }
                        };
                        serverCheck.setPeriod(Duration.seconds(SERVER_CHECK_INTERVAL));
                        serverCheck.start();


                        if (SystemTray.isSupported()) {
                            systemTrayThread = new Thread( () -> createSystemTrayThread() );
                            systemTrayThread.start();
                        }
                    }
                });


                f.channel().closeFuture().sync();

            } finally {

                group.shutdownGracefully().sync();
            }


        } catch(InterruptedException exc) {
            exc.printStackTrace();
        }

        final EventLoopGroup fxGroup = new NioEventLoopGroup();
        Bootstrap fxb = new Bootstrap();
        fxb.group(fxGroup)
                .channel(NioSocketChannel.class)
                .remoteAddress(new InetSocketAddress("localhost", 15001))
                .handler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel ch) throws Exception {
                        ch.pipeline().addLast(new TrayClientHandler());
                    }
                });
        fxf = fxb.connect();
    }

    private void loadAppIconSet() {
        appIconSet.put("red", createFXImage("/images/bkcourse_trayapp_indicator_red_16x16.png", "red tray icon"));
        appIconSet.put("yellow", createFXImage("/images/bkcourse_trayapp_indicator_yellow_16x16.png", "yellow tray " +
                "icon"));
        appIconSet.put("green", createFXImage("/images/bkcourse_trayapp_indicator_green_16x16.png", "green tray icon"));
        appIconSet.put("gray", createFXImage("/images/bkcourse_trayapp_indicator_gray_16x16.png", "gray tray icon"));
    }

    private static final void loadTrayIconSet() {

        String osName = System.getProperty("os.name");

        if( osName != null && osName.startsWith("Windows") ) {

            trayIconSet.put( "red", createImage("/images/bkcourse_trayapp_indicator_red_16x16.png", "red tray icon") );
            trayIconSet.put( "yellow", createImage("/images/bkcourse_trayapp_indicator_yellow_16x16.png", "yellow tray icon") );
            trayIconSet.put( "green", createImage("/images/bkcourse_trayapp_indicator_green_16x16.png", "green tray icon") );
            trayIconSet.put( "gray", createImage("/images/bkcourse_trayapp_indicator_gray_16x16.png", "gray tray icon") );


        } else if( osName != null && osName.startsWith("Mac") ) {

            trayIconSet.put( "red", createImage("/images/bkcourse_trayapp_indicator_red_20x20.png", "red tray icon") );
            trayIconSet.put( "yellow", createImage("/images/bkcourse_trayapp_indicator_yellow_20x20.png", "yellow tray icon") );
            trayIconSet.put( "green", createImage("/images/bkcourse_trayapp_indicator_green_20x20.png", "green tray icon") );
            trayIconSet.put( "gray", createImage("/images/bkcourse_trayapp_indicator_gray_20x20.png", "gray tray icon") );

        } else {  // default to Linux which is the largest

            trayIconSet.put( "red", createImage("/images/bkcourse_trayapp_indicator_red_24x24.png", "red tray icon") );
            trayIconSet.put( "yellow", createImage("/images/bkcourse_trayapp_indicator_yellow_24x24.png", "yellow tray icon") );
            trayIconSet.put( "green", createImage("/images/bkcourse_trayapp_indicator_green_24x24.png", "green tray icon") );
            trayIconSet.put( "gray", createImage("/images/bkcourse_trayapp_indicator_gray_24x24.png", "gray tray icon") );

        }
    }

    private void createSystemTrayThread() {

        loadTrayIconSet();

        final EventLoopGroup sb_group = new NioEventLoopGroup();
        try {
            try {

                ServerBootstrap sb = new ServerBootstrap();
                sb.group(sb_group).channel(NioServerSocketChannel.class)
                        .localAddress(new InetSocketAddress(15001))
                        .childHandler(new ChannelInitializer<SocketChannel>() {
                            @Override
                            protected void initChannel(SocketChannel ch) throws Exception {
                                ch.pipeline().addLast(new TrayServerHandler());
                            }
                        });

                ChannelFuture sb_f = sb.bind().sync();
                sb_f.channel().closeFuture().sync();

            }  finally {
                sb_group.shutdownGracefully().sync();
            }
        } catch (InterruptedException exc) {
            exc.printStackTrace();
        }

        final EventLoopGroup group = new NioEventLoopGroup();
        Bootstrap b = new Bootstrap();
        b.group(group)
                .channel(NioSocketChannel.class)
                .remoteAddress(new InetSocketAddress("localhost", 15000))
                .handler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel ch) throws Exception {
                        ch.pipeline().addLast(new TrayClientHandler());
                    }
                });

        final PopupMenu popup = new PopupMenu();

        final TrayIcon trayIcon =
                new TrayIcon( trayIconSet.get("gray") );
        final SystemTray tray = SystemTray.getSystemTray();

        MenuItem exitItem = new MenuItem("Exit");

        exitItem.addActionListener((evt) -> {

            try {

                ChannelFuture f = b.connect().sync();

                ByteBuf buf = Unpooled.copiedBuffer("exit".getBytes());
                f.channel().writeAndFlush(buf).sync();
                f.channel().closeFuture().sync();

                group.shutdownGracefully().sync();

            } catch(InterruptedException exc) {
                exc.printStackTrace();
            }
        });

        popup.add(exitItem);

        //trayIcon.setImageAutoSize(true);
        trayIcon.setPopupMenu(popup);

        try {
            tray.add(trayIcon);
        } catch (AWTException e) {
            System.out.println("TrayIcon could not be added.");
        }
    }

    protected static Image createImage(String path, String description) {
        URL imageURL = TrayApp.class.getResource(path);
        BufferedImage image = null;

        if (imageURL == null) {

            System.err.println("Resource not found: " + path);

        } else {

            try {
                image = ImageIO.read(imageURL);
            } catch(Exception exc) {
                exc.printStackTrace();
            }
        }

        return image;
    }

    protected javafx.scene.image.Image createFXImage(String path, String description) {
        return new javafx.scene.image.Image( path );
    }

    private static boolean isAlive() {

        try {
//            Request.Get("https://www.bekwam.com").execute().returnContent();
            Request.Get("http://localhost").execute().returnContent();
            missesCounter = 0;
        } catch(Exception exc) {
            missesCounter++;
            return false;
        }

        return true;
    }
}
