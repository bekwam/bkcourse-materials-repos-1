/*
 * Copyright 2016 Bekwam, Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bekwam.bkcourse.simpletableapp;

import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * @author carl
 */
public class SimpleTableController {

    @FXML
    TableColumn<Participant, String> tcScreenName;

    @FXML
    TableView<Participant> tblParticipants;

    @FXML
    public void initialize() {

        tcScreenName.setCellValueFactory(new PropertyValueFactory<Participant,String>("screenName"));
    }

    @FXML
    public void refresh() {

        Task<List<Participant>> task = new Task<List<Participant>>() {
            @Override
            protected List<Participant> call() throws Exception {
                return fetchData();
            }

            @Override
            protected void succeeded() {
                tblParticipants.getItems().clear();
                tblParticipants.getItems().addAll( fetchData() );
            }
        };

        new Thread(task).start();
    }

    private List<Participant> fetchData() {

        List<Participant> participantList = new ArrayList<>();

        participantList.add( new Participant("WALK" + counter++) );
        participantList.add( new Participant("DAVI" + counter++) );
        participantList.add( new Participant("JNYG" + counter++) );

        return participantList;
    }

    private int counter = 1;
}
