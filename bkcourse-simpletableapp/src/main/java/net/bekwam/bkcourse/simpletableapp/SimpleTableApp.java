/*
 * Copyright 2016 Bekwam, Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bekwam.bkcourse.simpletableapp;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * @author carl
 */
public class SimpleTableApp extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {

        VBox vbox = new VBox();
        vbox.setAlignment(Pos.CENTER);

        Button btnShowSimpleTable = new Button("Launch");
        btnShowSimpleTable.setOnAction( this::showSimpleTable );
        vbox.getChildren().add( btnShowSimpleTable );

        Scene scene = new Scene( vbox );

        primaryStage.setTitle("SimpleTableApp");
        primaryStage.setWidth( 480 );
        primaryStage.setHeight( 320 );
        primaryStage.setScene( scene );
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

    private void showSimpleTable(ActionEvent evt) {

        try {

            FXMLLoader fxmlLoader = new FXMLLoader(this.getClass().getResource("/SimpleTable.fxml"));

            Parent p = fxmlLoader.load();

            SimpleTableController c = fxmlLoader.getController();

            Scene scene = new Scene( p );

            Stage stage = new Stage();
            stage.setTitle( "SimpleTableApp - TableView");
            stage.setWidth( 667 );
            stage.setHeight( 375 );
            stage.setScene(scene);
            stage.setOnShown( (windowEvent) -> c.refresh() );
            stage.show();

        } catch(Exception exc) {
            exc.printStackTrace();
        }
    }
}
