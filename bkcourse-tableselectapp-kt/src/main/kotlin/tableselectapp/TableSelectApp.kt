package tableselectapp

import javafx.beans.binding.Bindings
import javafx.beans.property.*
import javafx.collections.FXCollections
import javafx.geometry.Insets
import javafx.scene.control.Button
import javafx.scene.control.TableView
import javafx.scene.control.TableView.CONSTRAINED_RESIZE_POLICY
import javafx.scene.layout.Priority
import tornadofx.*

/**
 * @author carl
 */

data class Item(val sku : StringProperty, val descr : StringProperty, val price : FloatProperty, val taxable :
BooleanProperty)

class TableSelectView : View("TableSelectApp") {

    private val items = FXCollections.observableArrayList(
        Item(SimpleStringProperty("KBD-0455892"), SimpleStringProperty("Mechanical Keyboard"), SimpleFloatProperty(100.0f), SimpleBooleanProperty(true)),
        Item(SimpleStringProperty("145256"), SimpleStringProperty("Product Docs"), SimpleFloatProperty(0.0f),
                SimpleBooleanProperty
        (false)),
        Item(SimpleStringProperty("OR-198975"), SimpleStringProperty("O-Ring (100)"), SimpleFloatProperty(10.0f),
                SimpleBooleanProperty
        (true))
    )

    var tblItems : TableView<Item> by singleAssign()
    var btnInventory : Button by singleAssign()
    var btnCalcTax : Button by singleAssign()

    override val root = vbox {
        tblItems = tableview(items) {

            column("SKU", Item::sku)
            column("Item", Item::descr)
            column("Price", Item::price)
            column("Taxable", Item::taxable)

            prefWidth = 667.0
            prefHeight = 376.0

            columnResizePolicy = CONSTRAINED_RESIZE_POLICY

            vboxConstraints {
                vGrow = Priority.ALWAYS
            }
        }
        hbox {
            btnInventory = button("Inventory")
            btnCalcTax = button("Tax")

            spacing = 8.0
        }

        padding = Insets(10.0)
        spacing = 10.0
    }

    init {

        btnInventory.disableProperty().bind( tblItems.selectionModel.selectedItemProperty().isNull )

        btnCalcTax.disableProperty().bind(
                tblItems.selectionModel.selectedItemProperty().isNull().or(
                    Bindings.select<Boolean>(
                            tblItems.selectionModel.selectedItemProperty(),
                            "taxable"
                    ).isEqualTo( false )
                )
        )
    }
}

class TableSelectApp : App(TableSelectView::class)