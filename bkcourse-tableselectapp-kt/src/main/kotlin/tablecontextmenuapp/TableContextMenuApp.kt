package tablecontextmenuapp

import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import javafx.geometry.Insets
import javafx.scene.control.ContextMenu
import javafx.scene.control.MenuItem
import javafx.scene.control.TableRow
import javafx.scene.layout.Priority
import javafx.util.Callback
import javafx.util.converter.LocalDateTimeStringConverter
import tornadofx.*
import java.time.LocalDateTime
import java.time.Month

    class Commit(commitDate : LocalDateTime, committer : String, logEntry : String) {

        val commitDateProperty = SimpleObjectProperty(commitDate)
        val committerProperty = SimpleStringProperty(committer)
        val logEntryProperty = SimpleStringProperty(logEntry)
    }

    class TableContextMenuController : Controller() {

        val data = mutableListOf(
            Commit(
                LocalDateTime.of(2018, Month.JANUARY, 3, 11, 15),
                    "walkerca",
                    "[#146] expose new user entity as a web service"),
            Commit(
                LocalDateTime.of(2018, Month.JANUARY, 2, 14, 45),
                    "walkerro",
                    "[#87] fixes table spacing glitch"),
            Commit(
                LocalDateTime.of(2018, Month.JANUARY, 1, 12, 0),
                    "walkerca",
                    "[#145] changes to support adding a user")
        ).observable()

        val consoleMessages = SimpleStringProperty("")
    }

class TableContextMenuView : View("Context Menu") {

    val c : TableContextMenuController by inject()

    override val root = vbox {

        tableview(c.data) {

            column("Date", Commit::commitDateProperty) {
                converter(LocalDateTimeStringConverter())
            }
            column("Committer", Commit::committerProperty)
            column("Log", Commit::logEntryProperty)

            rowFactory = Callback {
                object : TableRow<Commit>() {
                    override fun updateItem(item: Commit?, empty: Boolean) {
                        super.updateItem(item, empty)
                        if( item != null && !empty ) {
                            val revertMenuItem = MenuItem("Revert")
                            revertMenuItem.action {
                                c.consoleMessages += "Reverting commit from ${item.commitDateProperty.value} by ${item
                                        .committerProperty.value} - ${item.logEntryProperty.value}\n"
                            }
                            this.contextMenu = ContextMenu(revertMenuItem)
                        } else {
                            this.contextMenu = null
                        }
                    }
                }
            }

            vgrow = Priority.ALWAYS
        }

        titledpane("Console") {
            textarea(c.consoleMessages)
        }

        spacing = 4.0
        padding = Insets(4.0)
        prefHeight = 414.0
        prefWidth = 736.0
    }
}

class TableContextMenuApp : App(TableContextMenuView::class)

fun main(args: Array<String>) {
    launch<TableContextMenuApp>(args)
}