/*
 * Copyright 2017 Bekwam, Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bekwam.bkcourse.tabapp;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.stage.Stage;

/**
 * @author carl
 */
public class TabApp extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {

        TabPane tabs = new TabPane();

        Tab tab1 = new Tab("Tab1");
        Tab tab2 = new Tab( "Tab2");
        Tab tab3 = new Tab( "Tab3" );

        tabs.getTabs().addAll( tab1, tab2, tab3 );

        tabs.getSelectionModel().selectedItemProperty().addListener(
                (obs,ov,nv) -> {
                    if( nv == tab1 ) {
                        System.out.println("one");
                    } else if( nv == tab2 ) {
                        System.out.println("two");
                    } else if( nv == tab3 ) {
                        System.out.println("three");
                    } else {
                        System.out.println("unrecognized tab");
                    }
                }

        );
        Scene scene = new Scene(tabs);

        primaryStage.setTitle( "TabApp");
        primaryStage.setScene( scene );
        primaryStage.setWidth( 320 );
        primaryStage.setHeight( 568 );
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
