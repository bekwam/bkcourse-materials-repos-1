package net.bekwam.bkcourse.stringifyapp;

import javafx.application.Application;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.util.stream.Collectors;

class Person {
    private final String name;
    public Person(String name) { this.name = name; }
    public String getName() { return name; }
}

public class StringifyApp extends Application {

    private int counter = 1;

    @Override
    public void start(Stage primaryStage) throws Exception {

        ObservableList<Person> persons = FXCollections.observableArrayList();
        persons.add( new Person("Carl") );
        persons.add( new Person("Edvin") );
        persons.add( new Person("Thomas") );

        ListProperty<Person> personsProperty = new SimpleListProperty<>(persons);

        VBox vbox = new VBox();
        Label label = new Label("Empty");
        Button b = new Button("Add One");
        b.setOnAction( evt -> personsProperty.get().add( new Person("Matt " + counter++)));
        Label numItems = new Label("0");

        vbox.setSpacing( 10.0d );
        vbox.setPadding( new Insets( 40.0d ));

        vbox.getChildren().addAll( label, b, numItems );

        Scene scene = new Scene( vbox, 480, 320 );

        primaryStage.setScene( scene );

        label.textProperty()
                .bind(
                        Bindings.createStringBinding(
                                () ->
                                        personsProperty.get().stream().map( Person::getName ).collect(Collectors.joining
                                        (", ")),
                                personsProperty)
                );


        numItems.textProperty().bind( personsProperty.sizeProperty().asString() );

        primaryStage.show();
    }

    public static void main(String[] args) {
        Application.launch(args);
    }
}