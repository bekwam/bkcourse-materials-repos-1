package net.bekwam.bkcourse.flatwinapp.part2.start;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Rectangle;
import javafx.stage.Popup;
import javafx.stage.Window;

/**
 * @author carl
 */
public class FlatWinController {

    @FXML
    public void close(MouseEvent evt) {

        ((Label)evt.getSource()).getScene().getWindow().hide();
    }

}
