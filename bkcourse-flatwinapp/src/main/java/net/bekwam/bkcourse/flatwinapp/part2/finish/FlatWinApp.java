package net.bekwam.bkcourse.flatwinapp.part2.finish;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import net.bekwam.bkcourse.flatwinapp.common.WindowsHack;

/**
 * Main entry point into the FlatWinApp
 *
 * The window displays, is closable, can be resized, and is moveable.
 *
 * @author carl
 */
public class FlatWinApp extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {

        Parent p = FXMLLoader.load( FlatWinApp.class.getResource("/part2/finish/FlatWin.fxml"));

        Scene scene = null;

        String osName = System.getProperty("os.name");
        if( osName != null && osName.startsWith("Windows") ) {

            //
            // Windows hack b/c unlike Mac and Linux, UNDECORATED doesn't include a shadow
            //
            scene = (new WindowsHack()).getShadowScene(p);
            primaryStage.initStyle(StageStyle.TRANSPARENT);

        } else {
            scene = new Scene( p );
            primaryStage.initStyle(StageStyle.UNDECORATED);
        }

        scene.getStylesheets().add("part2/finish/fw.css");

        primaryStage.setTitle("flatwinapp");
        primaryStage.setScene( scene );
        primaryStage.setMinHeight(200.0d);
        primaryStage.setMinWidth(300.0d);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}

