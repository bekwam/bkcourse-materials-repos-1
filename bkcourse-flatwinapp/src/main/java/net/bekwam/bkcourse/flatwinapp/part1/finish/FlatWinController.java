package net.bekwam.bkcourse.flatwinapp.part1.finish;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;

/**
 * @author carl
 */
public class FlatWinController {

    @FXML
    public void close(MouseEvent evt) {

        ((Label)evt.getSource()).getScene().getWindow().hide();
    }

}
