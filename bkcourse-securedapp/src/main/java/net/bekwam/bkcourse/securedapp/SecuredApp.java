/*
 * Copyright 2016 Bekwam, Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bekwam.bkcourse.securedapp;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * @author carl
 */
public class SecuredApp extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {

        VBox vbox = new VBox();
        vbox.setPadding( new Insets(40.0d) );
        vbox.setSpacing( 4 );

        Label label = new Label("Select role");

        HBox hbox = new HBox();
        hbox.setSpacing( 4 );

        ObservableList<String> roles = FXCollections.observableArrayList("Admin", "User");
        ComboBox<String> cbRoles = new ComboBox<>(roles);
        cbRoles.setValue( "User ");

        Button launchButton = new Button("Launch");
        launchButton.setOnAction( (evt) -> launch( cbRoles.getSelectionModel().getSelectedItem()) );

        hbox.getChildren().addAll( cbRoles, launchButton );

        vbox.getChildren().addAll( label, hbox );

        Scene scene = new Scene(vbox);

        primaryStage.setTitle( "SecuredApp" );
        primaryStage.setScene( scene );
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

    private void launch(String role)  {

        try {
            FXMLLoader fxmlLoader = new FXMLLoader(SecuredApp.class.getResource("/MainView.fxml"));

            Parent p = fxmlLoader.load();

            MainViewController c = fxmlLoader.getController();

            Scene scene = new Scene(p);

            Stage stage = new Stage();
            stage.setTitle("Main View");
            stage.setScene(scene);
            stage.setWidth( 320 );
            stage.setHeight( 480 );
            stage.setOnShown( (winEvt) -> {
                c.setRole( role );
            });
            stage.show();

        } catch(Exception exc) {
            exc.printStackTrace();
        }
    }

}
