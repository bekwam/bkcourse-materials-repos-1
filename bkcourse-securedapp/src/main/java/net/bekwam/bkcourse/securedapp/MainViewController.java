/*
 * Copyright 2016 Bekwam, Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bekwam.bkcourse.securedapp;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.fxml.FXML;
import javafx.scene.layout.VBox;

/**
 * @author carl
 */
public class MainViewController {

    @FXML
    private VBox outerPanel, adminPanel;

    private StringProperty role = new SimpleStringProperty( "" );

    @FXML
    public void initialize() {

        // default view is user
        outerPanel.getChildren().remove( adminPanel );

        role.addListener(
                (obs,ov,nv) -> {
                    if( nv != null && nv.equals("Admin") ) {
                        if( !outerPanel.getChildren().contains( adminPanel ) ) {
                            outerPanel.getChildren().add( adminPanel );
                        }
                    } else {
                        if( outerPanel.getChildren().contains( adminPanel ) ) {
                            outerPanel.getChildren().remove( adminPanel );
                        }
                    }
                }
        );
    }

    public void setRole(String role) { this.role.set(role); }

}
