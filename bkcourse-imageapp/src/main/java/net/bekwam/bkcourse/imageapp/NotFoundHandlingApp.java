/*
 * Copyright 2017 Bekwam, Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bekwam.bkcourse.imageapp;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * Demonstrates what I consider a bug
 *
 * A non-existent URL does not throw an Exception the way a missing classpath
 * resource would.  The error and exception properties are also not set.
 *
 * @author carl
 */
public class NotFoundHandlingApp extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {

        Image goodImage = new Image("https://www.bekwam.com/images/bekwam_logo_hdr_rounded.png", true);
        goodImage.progressProperty().addListener((obs,ov,nv) -> System.out.println("progress=" + nv));

        ImageView goodImageView = new ImageView(goodImage);

        Image missingImage = new Image("https://www.bekwam.net/IMAGENOTFOUND.png", true);

        // not called
        missingImage.errorProperty().addListener(
                (obs,ov,nv) -> System.out.println("error=" + nv)
        );

        // not called also
        missingImage.exceptionProperty().addListener(
                (obs,ov,nv) -> System.out.println("exception=" + nv.getClass() + ", msg=" + nv.getMessage())
        );

        ImageView missingImageView = new ImageView(missingImage);

        VBox vbox = new VBox(goodImageView, missingImageView);

        Scene scene = new Scene(vbox);

        primaryStage.setScene( scene );
        primaryStage.show();
    }

    public static void main(String[] args) { launch(args); }
}
