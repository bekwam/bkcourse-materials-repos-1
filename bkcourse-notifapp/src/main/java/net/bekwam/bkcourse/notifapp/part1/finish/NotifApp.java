/*
 * Copyright 2016 Bekwam, Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bekwam.bkcourse.notifapp.part1.finish;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * @author carl
 */
public class NotifApp extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {

        FXMLLoader fxmlLoader = new FXMLLoader( NotifApp.class.getResource("/part1/finish/Notif.fxml"));

        Parent p = fxmlLoader.load();

        NotifController c = fxmlLoader.getController();

        Scene scene = new Scene( p );
        scene.getStylesheets().add( "part1/finish/notif.css");

        primaryStage.setTitle("NotifApp");
        primaryStage.setScene( scene );
        primaryStage.setWidth( 1024 );
        primaryStage.setHeight( 768 );
        primaryStage.setMinHeight( 200 );
        primaryStage.setMinWidth( 300 );
        primaryStage.show();

    }

    public static void main(String[] args) {
        launch(args);
    }

}
