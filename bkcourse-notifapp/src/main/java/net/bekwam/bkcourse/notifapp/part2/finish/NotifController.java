/*
 * Copyright 2016 Bekwam, Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bekwam.bkcourse.notifapp.part2.finish;

import javafx.animation.FadeTransition;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.When;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.util.Duration;
import org.apache.http.client.fluent.Content;
import org.apache.http.client.fluent.Request;

import java.util.LinkedList;
import java.util.Queue;

/**
 * @author carl
 */
public class NotifController {

    public static final int NUM_URLS = 3;
    public static final int MAX_NUM_DISPLAY_NOTIFS = 5;

    @FXML
    VBox contentContainer, notifContainer;

    //
    // One row in the UI is a Label title (ex, "Bekwam.com"), a TextField for the URL, a Label for the operation result,
    // and a ProgressIndicator
    //
    @FXML
    Label lblURLTitle1, lblURLTitle2, lblURLTitle3;

    @FXML
    Label lblStatus1, lblStatus2, lblStatus3;

    @FXML
    TextField tfURL1, tfURL2, tfURL3;

    @FXML
    ProgressIndicator piURL1, piURL2, piURL3;

    //
    // Several arrays that organize the UI components into a manner that's easy to iterate over
    //
    private final Label[] lblURLTitles = new Label[NUM_URLS];
    private final Label[] lblStatuses = new Label[NUM_URLS];
    private final TextField[] tfURLs = new TextField[NUM_URLS];
    private final ProgressIndicator[] piURLs = new ProgressIndicator[NUM_URLS];

    //
    // Keep track of any notifications that came in while the max number was already displayed.
    //
    private final Queue<StackPane> notifBacklog = new LinkedList<>();

    @FXML
    public void initialize() {

        initDisplay();
        setupArrays();

        //
        // Collapse the width so that it doesn't block the TextFields if nothing is showing.
        //
        notifContainer.maxWidthProperty().bind(
                new When(Bindings.createBooleanBinding(
                        () -> notifContainer.getChildren().isEmpty(), notifContainer.getChildren())
                ).
                        then( 0.0d ).
                        otherwise( 300.0d )
        );

    }

    private void initDisplay() {
        piURL1.setVisible(false);
        piURL2.setVisible(false);
        piURL3.setVisible(false);

        lblStatus1.setText("");
        lblStatus2.setText("");
        lblStatus3.setText("");

        notifContainer.getChildren().clear();
    }

    private void setupArrays() {

        lblURLTitles[0] = lblURLTitle1;
        lblURLTitles[1] = lblURLTitle2;
        lblURLTitles[2] = lblURLTitle3;

        lblStatuses[0] = lblStatus1;
        lblStatuses[1] = lblStatus2;
        lblStatuses[2] = lblStatus3;

        tfURLs[0] = tfURL1;
        tfURLs[1] = tfURL2;
        tfURLs[2] = tfURL3;

        piURLs[0] = piURL1;
        piURLs[1] = piURL1;
        piURLs[2] = piURL1;
    }

    private StackPane createNotif(String text ) {

        StackPane notifSP = new StackPane();
        notifSP.getStyleClass().add( "notif-sp" );

        VBox notifVBox = new VBox();
        notifVBox.getStyleClass().add( "notif-vbox" );

        Label notif = new Label(text);
        notif.getStyleClass().add( "notif-message" );
        notifVBox.getChildren().add( notif );

        Label closeButton = new Label("X");
        closeButton.setPadding(new Insets(2));
        closeButton.getStyleClass().add( "notif-close-button" );
        closeButton.setOnMouseClicked( (evt) -> {

            FadeTransition ft = new FadeTransition();
            ft.setDuration(Duration.millis(750) );
            ft.setNode( notifSP );
            ft.setFromValue( 1.0d );
            ft.setToValue( 0.1d );
            ft.setOnFinished( (evt2) -> {
                notifContainer.getChildren().remove(notifSP);
                if( !notifBacklog.isEmpty() ) {
                    notifContainer.getChildren().add( notifBacklog.poll() );
                }
            } );

            ft.play();

        });
        StackPane.setAlignment(closeButton, Pos.TOP_RIGHT);
        StackPane.setMargin( closeButton, new Insets(10));

        notifSP.getChildren().addAll( notifVBox, closeButton );

        return notifSP;
    }

    @FXML
    public void testAll() {

        for(int i = 0; i< NUM_URLS; i++ ) {

            String url_s = tfURLs[i].getText();

            if( url_s != null && !url_s.isEmpty() ) {

                final int index = i;
                Task<Long> task = new Task<Long>() {

                    @Override
                    protected Long call() throws Exception {

                        updateProgress( 0.1d, 1.0d );
                        long startMillis = System.currentTimeMillis();

                        Request.Get(url_s).execute().returnContent();

                        long endMillis = System.currentTimeMillis();
                        updateProgress( 1.0d, 1.0d );

                        return endMillis - startMillis;
                    }

                    @Override
                    protected void succeeded() {
                        super.succeeded();
                        lblStatuses[index].setText("Last check: " + getValue() + " ms");
                    }

                    @Override
                    protected void failed() {
                        super.failed();

                        lblStatuses[index].setText("Last check: error");

                        StackPane notifSP = createNotif( getException().getClass().getName() + " " + getException().getMessage() );

                        VBox.setMargin(notifSP, new Insets(10));

                        if( notifContainer.getChildren().size() < MAX_NUM_DISPLAY_NOTIFS ) {
                            notifContainer.getChildren().add(notifSP);
                        } else {
                            notifBacklog.add( notifSP );
                        }
                    }
                };

                piURLs[i].visibleProperty().bind( task.runningProperty() );

                new Thread(task).start();
            }
        }
    }
}
