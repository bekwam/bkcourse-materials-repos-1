/*
 * Copyright 2016 Bekwam, Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bekwam.bkcourse.notifapp.part1.finish;

import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextField;
import org.apache.http.client.fluent.Request;

/**
 * @author carl
 */
public class NotifController {

    public static final int NUM_URLS = 3;

    //
    // One row in the UI is a Label title (ex, "Bekwam.com"), a TextField for the URL, a Label for the operation result,
    // and a ProgressIndicator
    //
    @FXML
    Label lblURLTitle1, lblURLTitle2, lblURLTitle3;

    @FXML
    Label lblStatus1, lblStatus2, lblStatus3;

    @FXML
    TextField tfURL1, tfURL2, tfURL3;

    @FXML
    ProgressIndicator piURL1, piURL2, piURL3;

    //
    // Several arrays that organize the UI components into a manner that's easy to iterate over
    //
    private final Label[] lblURLTitles = new Label[NUM_URLS];
    private final Label[] lblStatuses = new Label[NUM_URLS];
    private final TextField[] tfURLs = new TextField[NUM_URLS];
    private final ProgressIndicator[] piURLs = new ProgressIndicator[NUM_URLS];

    @FXML
    public void initialize() {

        initDisplay();
        setupArrays();
    }

    private void initDisplay() {
        piURL1.setVisible(false);
        piURL2.setVisible(false);
        piURL3.setVisible(false);

        lblStatus1.setText("");
        lblStatus2.setText("");
        lblStatus3.setText("");
    }

    private void setupArrays() {

        lblURLTitles[0] = lblURLTitle1;
        lblURLTitles[1] = lblURLTitle2;
        lblURLTitles[2] = lblURLTitle3;

        lblStatuses[0] = lblStatus1;
        lblStatuses[1] = lblStatus2;
        lblStatuses[2] = lblStatus3;

        tfURLs[0] = tfURL1;
        tfURLs[1] = tfURL2;
        tfURLs[2] = tfURL3;

        piURLs[0] = piURL1;
        piURLs[1] = piURL1;
        piURLs[2] = piURL1;
    }

    @FXML
    public void testAll() {

        for(int i = 0; i< NUM_URLS; i++ ) {

            String url_s = tfURLs[i].getText();

            if( url_s != null && !url_s.isEmpty() ) {

                final int index = i;
                Task<Long> task = new Task<Long>() {

                    @Override
                    protected Long call() throws Exception {

                        updateProgress( 0.1d, 1.0d );
                        long startMillis = System.currentTimeMillis();

                        Request.Get(url_s).execute().returnContent();

                        long endMillis = System.currentTimeMillis();
                        updateProgress( 1.0d, 1.0d );

                        return endMillis - startMillis;
                    }

                    @Override
                    protected void succeeded() {
                        super.succeeded();
                        lblStatuses[index].setText("Last check: " + getValue() + " ms");
                    }

                    @Override
                    protected void failed() {
                        super.failed();
                        lblStatuses[index].setText("Last check: error");
                    }
                };

                piURLs[i].visibleProperty().bind( task.runningProperty() );

                new Thread(task).start();
            }
        }
    }
}
