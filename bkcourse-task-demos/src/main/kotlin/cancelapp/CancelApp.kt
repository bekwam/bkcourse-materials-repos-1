package cancelapp

import com.sun.glass.ui.Application
import javafx.concurrent.Task
import javafx.concurrent.Worker
import javafx.geometry.Insets
import javafx.geometry.Pos
import javafx.scene.control.Alert
import javafx.scene.image.Image
import javafx.scene.image.ImageView
import javafx.scene.layout.Priority
import tornadofx.*
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.net.URL

class CancelView : View("Cancel App") {

    val URL_BASE = "https://courses.bekwam.net/public_tutorials/images/"

    val imageList = listOf(
            "bkcourse_rc_charging.png",
            "bkcourse_rc_circuit.JPG",
            "bkcourse_rc_circuit_annotated.png",
            "bkcourse_rc_circuit_schem.png",
            "bkcourse_rc_charging.png",
            "bkcourse_greatgrid_fx.png",
            "bkcourse_greatgrid_spec.png",
            "bkcourse_greatgrid_swing.png",
            "bkcourse_flatwinapp_ea.png",
            "bkcourse_flatwinapp_final.png",
            "bkcourse_flatwinapp_initial_hier.png",
            "bkcourse_flatwinapp_mpu_gtk.png",
            "bkcourse_flatwinapp_mpu_mac.png",
            "bkcourse_flatwinapp_mpu_win.png",
            "bkcourse_flatwinapp_snagit.png",
            "bkcourse_kstudent_001_badcomment.png",
            "bkcourse_kstudent_001_mycomment.png",
            "bkcourse_kstudent_001_nocomment.png",
            "bkcourse_kstudent_001_println_docs.png",
            "bkcourse_kstudent_001_trykotlinlang.png",
            "bkcourse_kstudent_001_twoprintlns.png",
            "bkcourse_kstudent_001_twoprintlns_results.png"
    )

    val imageViews = mutableListOf<ImageView>().observable()

    val status : TaskStatus by inject()

    var runningTask : Task<MutableList<ByteArray>>? = null

    override val root = vbox {

        vbox {
            button("Fetch Images") {
                disableWhen { status.running }
                action {
                    runningTask = runAsync {
                        val imageBytesList = mutableListOf<ByteArray>()
                        for( (index,url) in imageList.withIndex() ) {
                            updateMessage("Loading $url...")
                            updateProgress( (index+1.0)/imageList.size, 1.0 )
                            if( !isCancelled ) {
                                imageBytesList.add(getBytes(URL_BASE + url))
                            }
                        }
                        imageBytesList
                    } ui {
                        addImages(it)
                    } fail {

                        val alert = Alert(Alert.AlertType.ERROR, it.message )
                        alert.headerText = "Error Loading Images"
                        alert.showAndWait()

                    } cancel {

                        val alert = Alert(Alert.AlertType.INFORMATION, "Operation Cancelled" )
                        alert.headerText = "Loading Images"
                        alert.showAndWait()

                    }
                }
            }
            scrollpane {
                flowpane {
                    children.bind( imageViews, { it } )

                    prefWidth = 667.0
                    prefHeight = 336.0

                    vgap = 10.0
                    hgap = 10.0
                }
                vgrow = Priority.ALWAYS
                padding = Insets(10.0)
            }

            vgrow = Priority.ALWAYS

            spacing = 10.0
            padding = Insets(10.0)
        }
        separator()
        hbox {
            visibleProperty().bind( status.running )
            progressbar(status.progress)
            button("Cancel") {
                action {
                    doCancel()
                }
            }
            label(status.message )

            alignment = Pos.CENTER_LEFT
            spacing = 4.0

            padding = Insets(0.0, 10.0, 10.0, 10.0)
        }

        spacing = 10.0

        prefWidth = 736.0
        prefHeight = 414.0
    }

    private fun doCancel() {
        if( runningTask != null && runningTask!!.isRunning() ) {
            runningTask!!.cancel()
        }
    }

    private fun addImages(imageBytesList : List<ByteArray>) {

        val images = imageBytesList.map {
            Image( ByteArrayInputStream(it), 200.0, 200.0, true, true )
        }

        imageViews.clear()
        imageViews.addAll( images.map { ImageView(it) })
    }

    private fun getBytes(url : String) : ByteArray {

        val BUF_SIZE = 100_000 // 100k
        val byteArray = ByteArray(BUF_SIZE)
        val baos = ByteArrayOutputStream()
        val byteStream = URL(url).openStream()

        try {
            var nbytes = byteStream.read(byteArray)
            baos.write(byteArray, 0, nbytes)

            while (nbytes > 0) {
                nbytes = byteStream.read(byteArray)
                if( nbytes > 0 ) {
                    baos.write(byteArray, 0, nbytes)
                }
            }

            return baos.toByteArray()

        } finally {
            byteStream.close()
            baos.close()
        }
    }
}

class CancelApp : App(CancelView::class)

fun main(args: Array<String>) {
    launch<CancelApp>(args)
}

// patch
infix fun <T> Task<T>.cancel(func: () -> Unit) = apply {
    fun attachCancelHandler() {
        if (state == Worker.State.CANCELLED) {
            func()
        } else {
            setOnCancelled {
                func()
            }
        }
    }

    if (Application.isEventThread())
        attachCancelHandler()
    else
        runLater { attachCancelHandler() }
}

