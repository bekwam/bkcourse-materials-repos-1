package snaptoapp

import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleIntegerProperty
import javafx.geometry.Insets
import javafx.geometry.Pos
import javafx.scene.Scene
import javafx.scene.layout.Pane
import javafx.scene.layout.Priority
import tornadofx.*

class SnapToView : View("Snap To App") {

    private val showGrid = SimpleBooleanProperty(true)
    private val snapToGrid = SimpleBooleanProperty(true)
    private val gridSize = SimpleIntegerProperty(10)

    private var drawingArea : Pane by singleAssign()

    override val root = vbox {
        hbox {
            checkbox("Show Grid", showGrid)
            checkbox( "Snap To", snapToGrid)
            label("Grid Size")
            textfield(gridSize) {
                isEditable = false
            }

            vboxConstraints {
                vgrow = Priority.NEVER
            }

            alignment = Pos.CENTER_LEFT
            padding = Insets(10.0)
            spacing = 10.0
        }
        drawingArea = pane {
            vboxConstraints {
                vgrow = Priority.ALWAYS
            }
        }
    }

    override fun onDock() {

    }
}

class SnapToApp : App(SnapToView::class) {

    override fun createPrimaryScene(view: UIComponent): Scene {
        return Scene(view.root, 1024.0, 768.0 )
    }
}