package listselectapp

import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleListProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import javafx.geometry.Insets
import javafx.geometry.Orientation
import javafx.geometry.Pos
import javafx.scene.control.Alert
import javafx.scene.control.SelectionMode
import tornadofx.*

class ListMultiSelectView : View("List Multi Select App") {

    val sensorReadings = mutableListOf(
            SensorData(0.0, 0L),
            SensorData(0.5, 1L),
            SensorData(1.0, 2L)
    ).observable()

    val selectedReadings = SimpleListProperty<SensorData>()

    val newVoltage = SimpleStringProperty()
    val newTime = SimpleStringProperty()

    override val root = vbox {
        listview(sensorReadings) {

            selectedReadings.bind( SimpleObjectProperty(this.selectionModel.selectedItems) )

            cellFormat {
                text = "${it.voltage}V (t=${it.time}ms)"  // the toString() doesn't have units
            }
            selectionModel.selectionMode = SelectionMode.MULTIPLE
        }
        hbox {
            label("New Voltage")
            textfield(newVoltage)
            label("At Time")
            textfield(newTime)
            button("Add") {
                enableWhen { newVoltage.isNotEmpty.and(newTime.isNotEmpty) }
                action {
                    sensorReadings.add(SensorData(newVoltage.value.toDouble(), newTime.value.toLong()))
                }
            }
            button("Clear") {
                enableWhen { newVoltage.isNotEmpty.or(newTime.isNotEmpty) }
                action {
                    newVoltage.value = ""
                    newTime.value = ""
                }
            }
            separator(Orientation.VERTICAL)
            button("Delete" ) {
                enableWhen { selectedReadings.emptyProperty().isEqualTo(SimpleBooleanProperty(false)) }
                action {

                    val toDelete = selectedReadings.value

                    alert(Alert.AlertType.INFORMATION,
                            "Deleted",
                            "Deleted ${toDelete}")

                    sensorReadings.removeAll(toDelete)
                }
            }
            alignment = Pos.CENTER_LEFT
            padding = Insets(10.0)
            spacing = 4.0
        }
    }
}

class ListMultiSelectApp : App(ListMultiSelectView::class)

fun main(args : Array<String>) {
    launch<ListMultiSelectApp>(args)
}