package listselectapp

import javafx.beans.property.SimpleDoubleProperty
import javafx.beans.property.SimpleLongProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import javafx.geometry.Insets
import javafx.geometry.Orientation
import javafx.geometry.Pos
import javafx.scene.control.Alert
import tornadofx.*

class SensorData(v : Double, t : Long) {
    val voltageProperty = SimpleDoubleProperty(v)
    var voltage by voltageProperty
    val timeProperty = SimpleLongProperty(t)
    var time by timeProperty

    override fun toString(): String = "$voltage (time=$time)"
}

class ListSelectView : View("List Select App") {

    val sensorReadings = mutableListOf(
            SensorData(0.0, 0L),
            SensorData(0.5, 1L),
            SensorData(1.0, 2L)
    ).observable()

    val selectedReading = SimpleObjectProperty<SensorData>()

    val newVoltage = SimpleStringProperty()
    val newTime = SimpleStringProperty()

    override val root = vbox {
        listview(sensorReadings) {
            bindSelected( selectedReading )
            cellFormat {
                text = "${it.voltage}V (t=${it.time}ms)"  // the toString() doesn't have units
            }
        }
        hbox {
            label("New Voltage")
            textfield(newVoltage)
            label("At Time")
            textfield(newTime)
            button("Add") {
                enableWhen { newVoltage.isNotEmpty.and(newTime.isNotEmpty) }
                action {
                    sensorReadings.add(SensorData(newVoltage.value.toDouble(), newTime.value.toLong()))
                }
            }
            button("Clear") {
                enableWhen { newVoltage.isNotEmpty.or(newTime.isNotEmpty) }
                action {
                    newVoltage.value = ""
                    newTime.value = ""
                }
            }
            separator(Orientation.VERTICAL)
            button("Delete" ) {
                enableWhen { selectedReading.isNotNull }
                action {

                    val obj = selectedReading.value

                    sensorReadings.remove(obj)

                    alert(Alert.AlertType.INFORMATION,
                            "Deleted",
                            "Deleted ${obj.voltage} at t=${obj.time}")

                }
            }
            alignment = Pos.CENTER_LEFT
            padding = Insets(10.0)
            spacing = 4.0
        }
    }
}

class ListSelectApp : App(ListSelectView::class)

fun main(args : Array<String>) {
    launch<ListSelectApp>(args)
}