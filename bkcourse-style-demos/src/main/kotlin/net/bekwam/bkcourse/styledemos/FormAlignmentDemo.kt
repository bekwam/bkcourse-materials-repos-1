package net.bekwam.bkcourse.styledemos

import javafx.geometry.Insets
import javafx.geometry.Orientation
import javafx.geometry.Pos
import tornadofx.*

class FormAlignmentDemoView : View("Form Alignment Demo") {

    override val root = hbox {
        form {
            fieldset {
                field("Arbeit") {
                    label("bei")
                    combobox<String>()
                    label("in")
                    combobox<String>()
                    label("als")
                    textfield()
                }

                field("Details") {
                    textarea()
                }
            }
        }
        separator(Orientation.VERTICAL)
        form {
            addClass( FormAlignmentDemoStylesheet.rightsideForm )
            fieldset {
                field("Arbeit") {
                    label("bei")
                    combobox<String>()
                    label("in")
                    combobox<String>()
                    label("als")
                    textfield()
                }

                field("Details") {
                    textarea()
                }
            }
        }
        padding = Insets(10.0)
        spacing = 4.0
    }
}

class FormAlignmentDemoStylesheet : Stylesheet() {

    companion object {
        val rightsideForm by cssclass()
    }

    init {
        rightsideForm {
            s(labelContainer, inputContainer) {
                alignment = Pos.TOP_LEFT
            }
        }
    }
}

class FormAlignmentDemo : App(FormAlignmentDemoView::class, FormAlignmentDemoStylesheet::class)

fun main(args: Array<String>) {  launch<FormAlignmentDemo>(args)  }