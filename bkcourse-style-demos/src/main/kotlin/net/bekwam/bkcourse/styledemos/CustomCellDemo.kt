package net.bekwam.bkcourse.styledemos

import javafx.beans.property.SimpleIntegerProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import javafx.scene.control.TableView.CONSTRAINED_RESIZE_POLICY
import javafx.scene.control.Tooltip
import javafx.scene.effect.BlurType
import javafx.scene.effect.DropShadow
import javafx.scene.paint.Color
import javafx.scene.shape.Circle
import tornadofx.*

enum class ServerStatus { UNKNOWN, DOWN, SLOW, UP }

class Server(region : String, status : ServerStatus = ServerStatus.UNKNOWN, lastPing : Int) {
    val regionProperty = SimpleStringProperty(region)
    val statusProperty = SimpleObjectProperty(status)
    val lastPingProperty = SimpleIntegerProperty(lastPing)
}

class CustomCellController : Controller() {
    val data = listOf(
            Server("NA-2", ServerStatus.UP, 56),
            Server("NA-3", ServerStatus.UP, 63),
            Server("NA-4", ServerStatus.UP, 63),
            Server("NA-5", ServerStatus.UP, 52),
            Server("EU-1", ServerStatus.UP, 40),
            Server("EU-2", ServerStatus.SLOW, 180),
            Server("EU-3", ServerStatus.SLOW, 179),
            Server("PA-1", ServerStatus.SLOW, 200),
            Server("PA-2", ServerStatus.DOWN, -1)
    ).observable()
}

class CustomCellView : View("Custom Cell Demo") {

    val controller : CustomCellController by inject()

    override val root = tableview(controller.data) {

        column("Region", Server::regionProperty)

        column("Status", Server::statusProperty) {
            cellFormat {
                if( it != null ) {

                    val circle = Circle(10.0)
                    circle.stroke = Color.BLACK
                    circle.strokeWidth = 2.0

                    when(it) {
                        ServerStatus.UNKNOWN -> circle.fill = c("gray")
                        ServerStatus.DOWN -> circle.fill = c("red")
                        ServerStatus.SLOW -> circle.fill = c("yellow")
                        ServerStatus.UP -> circle.fill = c("green")
                    }

                    if( it == ServerStatus.UNKNOWN || it == ServerStatus.DOWN) {
                        text = null
                        this.tooltip = Tooltip("Server Unreachable")
                    } else {
                        text = "${this.rowItem.lastPingProperty.value} ms"
                        this.tooltip = null
                    }

                    graphic = circle
                } else {
                    graphic = null
                    tooltip = null
                    text = null
                }
            }
        }

        columnResizePolicy = CONSTRAINED_RESIZE_POLICY
    }
}

class CustomCellDemo : App(CustomCellView::class)

fun main(args: Array<String>) {
    launch<CustomCellDemo>(args)
}