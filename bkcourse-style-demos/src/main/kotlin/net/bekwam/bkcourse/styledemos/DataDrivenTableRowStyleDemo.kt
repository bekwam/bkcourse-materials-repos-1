package net.bekwam.bkcourse.styledemos

import javafx.beans.property.SimpleIntegerProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import javafx.scene.control.TableView.CONSTRAINED_RESIZE_POLICY
import tornadofx.*
import java.time.LocalDate
import java.time.Month

class Subscription(id : Int, productName : String, renewalDate : LocalDate) {
    val idProperty = SimpleIntegerProperty(id)
    val productNameProperty = SimpleStringProperty(productName)
    val renewalDateProperty = SimpleObjectProperty(renewalDate)
}

class DataDrivenTableRowStyleView : View("Data Driven TableRow Style") {

    val data = listOf(
            Subscription( 555666, "Firmware Update", LocalDate.of(2019, Month.DECEMBER, 31) ),
            Subscription(9990, "Linux Distro", LocalDate.of(2018, Month.JANUARY, 1) ),
            Subscription( 17777, "Software App", LocalDate.of(2020, Month.MARCH, 15) ),
            Subscription( 20001, "Compression Utility", LocalDate.of(2019, Month.JULY, 1) )
    ).observable()

    override val root = tableview(data) {

    column("ID", Subscription::idProperty)
    column("Product", Subscription::productNameProperty)
    column("Renewal", Subscription::renewalDateProperty) {
        cellFormat {
            text = it.toString()
            if( it.isBefore(LocalDate.now())) {
                addClass(DDTRS_Styles.overdue)
                if( this.tableRow != null ) {
                    this.tableRow.addClass(DDTRS_Styles.highlighted)
                }
            } else {
                removeClass(DDTRS_Styles.overdue)
                if( this.tableRow != null ) {
                    this.tableRow.removeClass(DDTRS_Styles.highlighted)
                }
            }
        }
    }

    columnResizePolicy = CONSTRAINED_RESIZE_POLICY

    prefWidth = 736.0
    prefHeight = 414.0
}
}

class DDTRS_Styles : Stylesheet() {

    companion object {
        val overdue by cssclass()
        val highlighted by cssclass()
    }

    init {
        tableView {
            tableRowCell {
                and( highlighted ) {
                    backgroundColor += c("yellow")
                }
            }
        }

        overdue {
            textFill = c("red")
            fill = c("blue")
        }
    }
}

class DataDrivenTableRowStyleDemo : App(DataDrivenTableRowStyleView::class, DDTRS_Styles::class)

fun main(args: Array<String>) {
    launch<DataDrivenTableRowStyleDemo>(args)
}