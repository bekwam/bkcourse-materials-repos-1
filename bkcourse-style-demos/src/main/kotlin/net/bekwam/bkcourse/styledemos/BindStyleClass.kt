package net.bekwam.bkcourse.styledemos

import javafx.beans.binding.Bindings
import javafx.beans.property.SimpleStringProperty
import javafx.geometry.Insets
import javafx.scene.Scene
import javafx.scene.control.ToggleGroup
import javafx.scene.paint.Color
import tornadofx.*
import java.util.concurrent.Callable

class BindStyleClassView : View("Bind Style Class") {

    var tg : ToggleGroup by singleAssign()

    val display = SimpleStringProperty()

    val displayStyle = Bindings.createObjectBinding(
            Callable {
                when(display.value) {
                    "Success" -> BSCStyles.success
                    "Error" -> BSCStyles.error
                    else -> BSCStyles.clear
                }
            },
            display
    )

    override val root = vbox{

        tg = togglegroup {
            bind( display )  // bind to selected toggle
        }

        hbox {
            radiobutton("Clear", tg) {
                isSelected = true
            }
            radiobutton("Success", tg)
            radiobutton("Error", tg)
            spacing = 2.0
        }

        textfield(display)
        label("Message") {
            bindClass(displayStyle)
        }

        padding = Insets(10.0)
        spacing = 10.0
    }
}

class BSCStyles : Stylesheet() {

    companion object {
        val clear by cssclass()
        val success by cssclass()
        val error by cssclass()
    }

    init {

        clear {
            textFill = Color.BLACK
        }

        success {
            textFill = Color.GREEN
        }

        error {
            textFill = Color.RED
        }
    }
}

class BindStyleClass : App(BindStyleClassView::class, BSCStyles::class) {
    override fun createPrimaryScene(view: UIComponent) = Scene(view.root, 568.0, 320.0)
}

fun main(args: Array<String>) {
    launch<BindStyleClass>(args)
}