package pollapp

import javafx.application.Application
import javafx.application.Platform
import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleStringProperty
import javafx.concurrent.ScheduledService
import javafx.concurrent.Task
import javafx.geometry.Insets
import javafx.geometry.Pos
import javafx.scene.Scene
import javafx.scene.layout.Priority
import javafx.util.Duration
import tornadofx.*
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

const val POLL_INTERVAL = 2.0

data class DataResult(val data : String, val lastRetrieve : LocalDateTime)

class PollView : View("Poll App") {

    val controller : PollController by inject()

    override val root = vbox {
        label(controller.url)
        textarea(controller.currentData) {
            vgrow = Priority.ALWAYS
        }
        hbox {
            button("Start") {
                disableProperty().bind( controller.stopped.not() )
                setOnAction {
                    controller.start()
                }
            }
            button("Stop") {
                disableProperty().bind( controller.stopped )
                setOnAction {
                    controller.stop()
                }
            }
            button("Exit") {
                setOnAction {
                    Platform.exit()
                }
            }
            hbox {
                label {
                    bind(
                        SimpleStringProperty("Last Updated: ")
                                .concat( controller.lastUpdated)
                    )
                }
                alignment = Pos.CENTER_RIGHT
                hgrow = Priority.ALWAYS
            }
            spacing = 4.0
            padding = Insets(4.0)
        }
        padding = Insets(10.0)
        spacing = 10.0
    }
}

class PollController : Controller() {

    val api : Rest by inject()

    val currentData = SimpleStringProperty()
    val stopped = SimpleBooleanProperty(true)
    val url = SimpleStringProperty(api.baseURI)
    val lastUpdated = SimpleStringProperty("")

    val scheduledService = object : ScheduledService<DataResult>() {
        init {
            period = Duration.seconds(POLL_INTERVAL)
        }
        override fun createTask() : Task<DataResult> = FetchDataTask()
    }

    fun start() {
        scheduledService.restart()
        stopped.value = false
    }

    fun stop() {
        scheduledService.cancel()
        stopped.value = true
    }

    inner class FetchDataTask : Task<DataResult>() {

        override fun call(): DataResult {

            val htmlBody = api.get("/")  // fake "recv"

            return DataResult(htmlBody.text()!!, LocalDateTime.now())
        }

        override fun succeeded() {
            println("data updated: " + value.lastRetrieve)
            lastUpdated.value = value.lastRetrieve.format(DateTimeFormatter.ISO_LOCAL_TIME)
            this@PollController.currentData.value = value.data
        }

        override fun failed() {
            println("failed retrieval")
            exception.printStackTrace()
        }
    }
}

class PollApp : App(PollView::class) {

    val api : Rest by inject()

    override fun createPrimaryScene(view: UIComponent): Scene =
            Scene(view.root, 736.0, 414.0)

    init {
        api.baseURI = "https://www.bekwam.com"
    }
}

fun main(args : Array<String>) {
    Application.launch( PollApp::class.java )
}

