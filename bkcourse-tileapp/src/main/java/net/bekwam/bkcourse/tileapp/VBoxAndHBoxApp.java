/*
 * Copyright 2016 Bekwam, Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bekwam.bkcourse.tileapp;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;

/**
 * @author carl
 */
public class VBoxAndHBoxApp extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {

        VBox vbox = new VBox();
        vbox.setAlignment( Pos.CENTER );

        Circle redCircle = new Circle(50, Color.RED);
        Circle greenCircle = new Circle( 50, Color.GREEN );

        HBox top = new HBox( redCircle, greenCircle );

        Circle blueCircle = new Circle( 50, Color.BLUE );
        Circle yellowCircle = new Circle( 50, Color.YELLOW );

        HBox bottom = new HBox(blueCircle, yellowCircle );

        vbox.getChildren().addAll(
                top,
                bottom
        );

        vbox.setOnMouseClicked( (evt) -> {
            for (Node n : vbox.getChildren()) {
                HBox hbox = (HBox) n;
                hbox
                        .getChildren()
                        .stream()
                        .filter(c ->
                            c.contains(
                                c.sceneToLocal(evt.getSceneX(), evt.getSceneY(), true)
                            )
                        )
                        .findFirst()
                        .ifPresent((c) -> c.setOpacity(0.7d));
            }
        });

        Scene scene = new Scene(vbox);

        primaryStage.setTitle("VBoxAndHBoxApp");
        primaryStage.setScene( scene );
        primaryStage.show();

    }

    public static void main(String[] args) {
        launch(args);
    }
}
