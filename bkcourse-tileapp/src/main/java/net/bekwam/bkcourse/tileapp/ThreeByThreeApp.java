/*
 * Copyright 2016 Bekwam, Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bekwam.bkcourse.tileapp;

import javafx.application.Application;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.layout.TilePane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

/**
 * @author carl
 */
public class ThreeByThreeApp extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {

        TilePane tilePane = new TilePane();
        tilePane.setPrefColumns(3);
        tilePane.setPrefRows(3);
        tilePane.setTileAlignment( Pos.CENTER );
        tilePane.setOrientation(Orientation.VERTICAL);
        tilePane.setPrefTileHeight(100);
        tilePane.setPrefTileWidth(100);
        tilePane.getChildren().addAll(
        		new Rectangle(50, 50, Color.RED),
        		new Rectangle( 50, 50, Color.GREEN ),
        		new Rectangle( 50, 50, Color.BLUE ),
        		new Rectangle( 50, 50, Color.YELLOW ),
        		new Rectangle( 50, 50, Color.CYAN )/*,
        		new Rectangle( 50, 50, Color.PURPLE ),
        		new Rectangle( 50, 50, Color.BROWN ),
        		new Rectangle( 50, 50, Color.PINK ),
        		new Rectangle( 50, 50, Color.ORANGE ) */
        );

        Scene scene = new Scene(tilePane);
        scene.setFill(Color.LIGHTGRAY);
        
        primaryStage.setTitle("3x3");
        primaryStage.setScene( scene );
        primaryStage.show();
    }

    public static void main(String[] args) {launch(args);}
}
