/*
 * Copyright 2016 Bekwam, Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bekwam.bkcourse.titledpaneapp;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * @author carl
 */
public class NonCollapsibleTitledPaneApp extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {

            VBox vbox = new VBox(
                    new Label("Filename"),
                    new TextField(),
                    new Label( "Size"),
                    new TextField()
            );
            vbox.setPadding( new Insets(10) );
            vbox.setSpacing( 10 );

            VBox securityVBox = new VBox(
                    new Label("Owner"),
                    new TextField(),
                    new Label("Access Control"),
                    new TextField()
            );

            TitledPane tp = new TitledPane("Security", securityVBox);
            tp.setCollapsible( false );

            vbox.getChildren().add( tp );

            Scene scene = new Scene(vbox);

            primaryStage.setTitle( "NonCollapsibleTitledPaneApp");
            primaryStage.setScene( scene );
            primaryStage.setWidth( 568 );
            primaryStage.setHeight( 320 );
            primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
