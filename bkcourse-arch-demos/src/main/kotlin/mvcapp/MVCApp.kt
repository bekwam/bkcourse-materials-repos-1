package mvcapp

import javafx.beans.property.SimpleStringProperty
import javafx.geometry.Insets
import javafx.geometry.Pos
import javafx.scene.layout.Priority
import tornadofx.*

class MVCController : Controller() {

    val form : MVCFormView by inject()
    val status : MVCStatusView by inject()

    fun save() {

        val dataToSave = form.data.value

        // execute save here
        println("Saving ${dataToSave}")

        status.lastMessage.value = "Data saved"
    }
}

class MVCView : View("MVC App") {

    val form : MVCFormView by inject()
    val status : MVCStatusView by inject()

    override val root = vbox {
        add(form)
        separator()
        add(status)

        prefWidth = 480.0
        prefHeight = 320.0

    }
}

class MVCFormView : View() {

    val data = SimpleStringProperty()

    override val root = form {
        fieldset {
            field("Data") {
                textfield(data)
            }
        }
        button("Save") {
            action {
                find<MVCController>().save()  // breaks by inject cycle
            }
        }

        padding = Insets(10.0)
        vgrow = Priority.ALWAYS
        alignment = Pos.CENTER_LEFT
    }
}

class MVCStatusView : View() {

    val lastMessage = SimpleStringProperty()

    override val root = hbox {
        label(lastMessage)

        padding = Insets(4.0)
        vgrow = Priority.NEVER
    }
}

class MVCApp : App(MVCView::class)

fun main(args: Array<String>) {
    launch<MVCApp>(args)
}