package mvvmapp

import javafx.beans.property.SimpleStringProperty
import javafx.geometry.Insets
import javafx.geometry.Pos
import javafx.scene.layout.Priority
import tornadofx.*

class DataSavedEvent(val message : String) : FXEvent()

class MVVMFormView : View() {

    val vm : MVVMFormViewModel by inject()

    override val root = form {
        fieldset {
            field("Data") {
                textfield(vm.data)
            }
        }
        button("Save") {
            action {
                vm.save()
            }
        }

        padding = Insets(10.0)
        vgrow = Priority.ALWAYS
        alignment = Pos.CENTER_LEFT
    }
}

class MVVMFormViewModel : ViewModel() {

    val data = SimpleStringProperty()

    fun save() {

        val dataToSave = data.value

        // execute save here
        println("Saving ${dataToSave}")

        fire(DataSavedEvent("Data saved"))
    }
}

class MVVMStatusView : View() {

    val vm : MVVMStatusViewModel by inject()

    override val root = hbox {
        label(vm.lastMessage)

        padding = Insets(4.0)
        vgrow = Priority.NEVER
    }
}

class MVVMStatusViewModel : ViewModel() {

    val lastMessage = SimpleStringProperty()

    init {
        subscribe<DataSavedEvent> {
            lastMessage.value = it.message
        }
    }
}

class MVVMView : View("MVVM App") {

    val form : MVVMFormView by inject()
    val status : MVVMStatusView by inject()

    override val root = vbox {
        add(form)
        separator()
        add(status)

        prefWidth = 480.0
        prefHeight = 320.0
    }
}

class MVVMApp : App(MVVMView::class)

fun main(args: Array<String>) {
    launch<MVVMApp>(args)
}