package ivmapp1

import javafx.beans.property.SimpleStringProperty
import tornadofx.*

data class Person ( val fn : String, val ln : String) {
    val firstNameProperty = SimpleStringProperty(fn)
    val lastNameProperty = SimpleStringProperty(ln)
}

class PersonViewModel : ItemViewModel<Person>() {
    val firstName = bind { item?.firstNameProperty }
    val lastName = bind { item?.lastNameProperty }

    override fun onCommit() {
        super.onCommit()
        println("Saving ${item}")
    }
}

class PersonsModel {
    val persons = listOf(
            Person("Carl", "Walker"),
            Person( "Dave", "Smith"),
            Person( "Jim", "James")
    ).observable()
}

class MasterView : View("Master View") {

    val model = PersonsModel()

    val vm : PersonViewModel by inject()

    override val root = tableview(model.persons) {
        column("First Name", Person::firstNameProperty)
        column("Last Name", Person::lastNameProperty)
        bindSelected(vm)
        columnResizePolicy = SmartResize.POLICY
    }
}

class DetailsView : View("Details View") {

    val vm : PersonViewModel by inject()

    override val root = form {
        fieldset {
            field("First Name") {
                textfield(vm.firstName)
            }
            field( "Last Name") {
                textfield(vm.lastName)
            }
        }
        button("Save") {
            action {
                vm.commit()
            }
        }
    }
}

class MainView : View("IVM App") {

    override val root = vbox {
        hbox {
            this += MasterView::class
            this += DetailsView::class
        }
    }
}

class IVMApp : App(MainView::class)

fun main(args: Array<String>) {
    launch<IVMApp>(args)
}

