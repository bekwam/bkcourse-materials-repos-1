package ivmapp2

import javafx.beans.property.SimpleIntegerProperty
import javafx.beans.property.SimpleStringProperty
import tornadofx.*

data class Person ( val fn : String, val ln : String) {

    val firstNameProperty = SimpleStringProperty(fn)
    val lastNameProperty = SimpleStringProperty(ln)

    val idProperty = SimpleIntegerProperty( nextId() )

    companion object {
        private var idgen = 1 // faux static class member
        fun nextId() = idgen++
    }
}

class PersonViewModel : ItemViewModel<Person>() {
    val firstName = bind { item?.firstNameProperty }
    val lastName = bind { item?.lastNameProperty }
}

class SavePersonEvent(val item : Person) : FXEvent()
class DeletePersonEvent(val item : Person) : FXEvent()

class PersonsModel : Component(), ScopedInstance {

    val persons = listOf(
            Person("Carl", "Walker"),
            Person( "Dave", "Smith"),
            Person( "Jim", "James")
    ).observable()

    init {
        subscribe<SavePersonEvent> {
            println("saving ${it.item}")
        }
        subscribe<DeletePersonEvent> {
            println("deleting ${it.item}")
        }
    }
}

class MasterView : View("Master View") {

    val model : PersonsModel by inject()

    val vm : PersonViewModel by inject()

    override val root = tableview(model.persons) {
        column("First Name", Person::firstNameProperty)
        column("Last Name", Person::lastNameProperty)
        columnResizePolicy = SmartResize.POLICY

        onDoubleClick {
            vm.item = selectedItem
        }
    }
}

class DetailsView : View("Details View") {

    val vm: PersonViewModel by inject()

    override val root = form {
        fieldset {
            field("First Name") {
                textfield(vm.firstName)
            }
            field("Last Name") {
                textfield(vm.lastName)
            }
        }
        buttonbar {
            button("New") {
                action {
                    vm.item = Person("", "")
                }
            }
            button("Save") {
                action {
                    println("pre-commit fields ${vm.firstName.value} ${vm.lastName.value}")
                    println("pre-commit item ${vm.item}")
                    vm.commit()
                    println("post-commit item ${vm.item}")
                    fire(SavePersonEvent(vm.item))
                }
            }
            button("Delete") {
                action {
                    vm.item = Person("", "")  // clear out field
                    fire(DeletePersonEvent(vm.item))
                }
            }
        }
    }
}

class MainView : View("IVM App") {

    override val root = vbox {
        hbox {
            this += MasterView::class
            this += DetailsView::class
        }
    }
}

class IVMApp : App(MainView::class)

fun main(args: Array<String>) {
    launch<IVMApp>(args)
}

